# PyTech Django Tools

![pipeline status](https://gitlab.com/pytech-srl/resources/pytech-django-tools/badges/main/pipeline.svg)
![coverage status](https://gitlab.com/pytech-srl/resources/pytech-django-tools/badges/main/coverage.svg)
![interrogate status](https://gitlab.com/pytech-srl/resources/pytech-django-tools/-/raw/main/interrogate_badge.svg)

## Description
This project aims to provide basic tools that may be useful in django projects.

## Roadmap
You may find some new features coming in the [Issue section](https://gitlab.com/pytech-srl/resources/pytech-django-tools/-/issues)

## Authors and acknowledgment
This package is provided thanks to:

- Alessandro Grandi (PyTech srl)
