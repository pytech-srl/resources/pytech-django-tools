import pytest
from django.contrib.auth.middleware import AuthenticationMiddleware
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseRedirect
from django.test import RequestFactory


@pytest.fixture
def default_django_user_password():
    """
    Fixture for default user password
    """
    return "some-password"


@pytest.fixture
def default_django_user(django_user_model, default_django_user_password):
    """
    Fixture for default django user
    """
    user = django_user_model.objects.create(
        email="someone@somewhere.it", password=default_django_user_password
    )
    user.set_password(default_django_user_password)
    user.save()
    return user


@pytest.fixture
def simple_http_request():
    """
    Fixture for a simple http get request
    """
    request = RequestFactory().get("/")
    return request


@pytest.fixture(name="post_data", params=[None])
def _post_data(request):
    """
    Fixture for custom post data
    :param request: fixture request for param access
    """
    return request.param


@pytest.fixture(name="post_data_by_name", params=["post_data"])
def _post_data_by_name(request):
    """
    Fixture for parametrized post data
    :param request: fixture request for param access
    """
    return request.getfixturevalue(request.param)


@pytest.fixture
def post_data_http_request(post_data_by_name):
    """
    Fixture for a simple http post request with parametrized payload
    :param post_data_by_name: post payload
    """
    request = RequestFactory().post("/", post_data_by_name)
    return request


@pytest.fixture(autouse=True)
def http_request_with_messages_and_authentication(simple_http_request):
    """
    Auto-use fixture used to apply django middlewares to the get request.
    Process request through MessageMiddleware in order to use messages feature
    SessionMiddleware is required by MessageMiddleware

    :param simple_http_request: a simple get request
    """
    SessionMiddleware(simple_http_request).process_request(simple_http_request)
    MessageMiddleware(simple_http_request).process_request(simple_http_request)
    AuthenticationMiddleware(simple_http_request).process_request(simple_http_request)


@pytest.fixture
def expected_http_response():
    """
    Fixture for the expected success http response
    """
    return HttpResponse("OK!")


@pytest.fixture
def simple_django_view(expected_http_response):
    """
    Fixture for a simple mocked django view that returns a success response
    :param expected_http_response: the expected success response
    :return: the mock view
    """
    def view(request):
        """
        The mock view
        :param request: the http request
        :return: a success response
        """
        return expected_http_response

    return view


@pytest.fixture
def simple_django_forbidden_view():
    """
    Fixture for a simple mocked django view that always raises a PermissionDenied error
    :return: the mock view
    """
    def view(request):
        """
        The mock view
        :param request: the http request
        :raise: PermissionDeinied
        """
        raise PermissionDenied("Permission denied error message")

    return view


@pytest.fixture
def simple_django_view_with_pk(expected_http_response):
    """
    Fixture for a simple mocked django view that returns a success response
    :param expected_http_response: the expected success response
    :return: the mock view
    """
    def view(request, pk):
        """
        The mock view
        :param request: the http request
        :param pk: the object primary key
        :return: a success response
        """
        return expected_http_response

    return view


@pytest.fixture
def simple_django_redirect_view():
    """
    Fixture for a simple mocked django view that returns a redirect response
    :return: the mock view
    """
    def view(request):
        """
        The mock view
        :param request: the http request
        :return: a redirect response
        """
        return HttpResponseRedirect(redirect_to="redirect/address")

    return view


@pytest.fixture
def simple_django_redirect_to_login_view():
    """
    Fixture for a simple mocked django view that returns a redirect responses
    to the login view
    :return: the mock view
    """
    def view(request):
        """
        The mock view
        :param request: the http request
        :return: a redirect response to the login view
        """
        return HttpResponseRedirect(redirect_to="/?next=my-custom-page")

    return view
