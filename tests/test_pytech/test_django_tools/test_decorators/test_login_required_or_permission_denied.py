import pytest
from django.core.exceptions import PermissionDenied

from pytech.django_tools.decorators import login_required_or_permission_denied


def test__no_effect__if_user_logged_in(
    simple_http_request, default_django_user, simple_django_view
):
    simple_http_request.user = default_django_user
    decorated_view = login_required_or_permission_denied(simple_django_view)

    response = simple_django_view(simple_http_request)
    response_decorated = decorated_view(simple_http_request)

    assert 200 == response.status_code == response_decorated.status_code
    assert response == response_decorated


def test__message_found__if_anonymous_user(simple_http_request, simple_django_view):
    decorated_view = login_required_or_permission_denied(
        simple_django_view,
    )

    with pytest.raises(PermissionDenied):
        decorated_view(simple_http_request)
