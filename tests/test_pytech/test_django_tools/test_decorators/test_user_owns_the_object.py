import pytest
from django.core.exceptions import PermissionDenied
from django.http import Http404


def get_test__raise_404__if_object_does_not_exist(decorator):
    """
    Test factory for custom decorators.
    Returns a test that raises a 404 error
    if the object corresponding to the pk does not exist

    :param decorator: the decorator to use
    :return: the custom test
    """
    @pytest.mark.django_db
    def test__raise_404__if_object_does_not_exist(
        simple_http_request, simple_django_view_with_pk
    ):
        decorated_view = decorator(simple_django_view_with_pk)

        with pytest.raises(Http404):
            decorated_view(simple_http_request, pk=0)

    return test__raise_404__if_object_does_not_exist


def get_test__raise_permission_denied__if_user_not_owner(decorator, model_instance):
    """
    Test factory for custom decorators.
    Returns a test that raises a PermissionDenied error
    if the object corresponding to the pk does not exist

    :param decorator: the decorator to use
    :param model_instance: the model bound to the decorated view
    :return: the custom test
    """

    @pytest.mark.django_db
    def test__raise_permission_denied__if_user_not_owner(
        default_django_user, simple_http_request, simple_django_view_with_pk
    ):
        decorated_view = decorator(simple_django_view_with_pk)

        with pytest.raises(PermissionDenied):
            decorated_view(simple_http_request, pk=model_instance.pk)

    return test__raise_permission_denied__if_user_not_owner


def get_test__user_can_access__if_user_is_owner(decorator, model_instance):
    """
    Test factory for custom decorators. Check that user can access the view
    if the user is the owner of the object referred by the view

    :param decorator: the decorator to use
    :param model_instance: the model bound to the decorated view
    :return: the custom test
    """

    @pytest.mark.django_db
    def test__user_can_access__if_user_is_owner(user, http_request, view):
        decorated_view = decorator(view)
        http_request.user = user

        response = decorated_view(http_request, pk=model_instance.pk)

        assert 200 == response.status_code

    return test__user_can_access__if_user_is_owner
