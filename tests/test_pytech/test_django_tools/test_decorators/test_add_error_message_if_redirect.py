import pytest
from django.contrib import messages
from django.core.exceptions import PermissionDenied

from pytech.django_tools.decorators import raise_permission_denied_if_redirect_to_login


def test__no_action_performed__if_no_redirect(simple_http_request, simple_django_view):
    decorated_django_view = raise_permission_denied_if_redirect_to_login(
        error_message=""
    )(simple_django_view)

    response = simple_django_view(simple_http_request)
    decorated_response = decorated_django_view(simple_http_request)

    # The same response is provided
    assert response == decorated_response
    assert response is decorated_response

    # No message is added
    message_list = list(map(str, messages.get_messages(request=simple_http_request)))
    assert not message_list


def test__permission_denied_not_raised__if_not_redirect(
    simple_http_request, simple_django_view
):
    error_message = "CUSTOM REDIRECT ERROR"
    decorated_view = raise_permission_denied_if_redirect_to_login(
        error_message=error_message
    )(simple_django_view)

    decorated_view(simple_http_request)


def test__permission_denied_not_raised__if_redirect_not_to_login_url(
    simple_http_request, simple_django_redirect_view
):
    error_message = "CUSTOM REDIRECT ERROR"
    decorated_view = raise_permission_denied_if_redirect_to_login(
        error_message=error_message
    )(simple_django_redirect_view)

    decorated_view(simple_http_request)


def test__permission_denied__if_redirect_to_login_url(
    simple_http_request, simple_django_redirect_to_login_view
):
    error_message = "CUSTOM REDIRECT ERROR"
    decorated_redirect_view = raise_permission_denied_if_redirect_to_login(
        error_message=error_message
    )(simple_django_redirect_to_login_view)

    with pytest.raises(PermissionDenied):
        decorated_redirect_view(simple_http_request)
