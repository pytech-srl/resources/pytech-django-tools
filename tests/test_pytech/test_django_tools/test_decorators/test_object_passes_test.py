import pytest
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.http import Http404

from pytech.django_tools.decorators import object_passes_test


@pytest.mark.parametrize(
    "model_class", [object, type, int, float, str, type("GenericClass", (object,), {})]
)
def test__raise_type_error_if_object_is_not_a_model(model_class):
    with pytest.raises(TypeError, match="not a valid Model"):
        object_passes_test(model_class, lambda: None)


@pytest.mark.django_db
def test__decorated_function_executed_if_test_passes(
    default_django_user, simple_django_view_with_pk
):
    decorator_test_pass = object_passes_test(get_user_model(), lambda obj: True)
    result = simple_django_view_with_pk(request=None, pk=default_django_user.pk)
    decorated_result = decorator_test_pass(simple_django_view_with_pk)(
        request=None, pk=default_django_user.pk
    )

    assert result == decorated_result


@pytest.mark.django_db
def test__raise_404_if_object_does_not_exist(simple_django_view_with_pk):
    decorator_test_pass = object_passes_test(get_user_model(), lambda obj: True)
    decorated_view = decorator_test_pass(simple_django_view_with_pk)

    with pytest.raises(Http404):
        decorated_view(None, pk=0)


@pytest.mark.django_db
def test__raise_permission_denied__if_test_does_not_pass(
    default_django_user, simple_http_request, simple_django_view_with_pk
):
    decorator_test_fail = object_passes_test(get_user_model(), lambda obj: False)
    decorated_view = decorator_test_fail(simple_django_view_with_pk)

    with pytest.raises(PermissionDenied):
        decorated_view(None, pk=default_django_user.pk)
